from django.contrib import admin
from django.urls import path
from .views import (
    api_list_customers,
    api_show_customer,
    api_list_sales,
    api_show_sale,
    api_list_reps,
    api_show_rep,
    api_list_autos
    )

urlpatterns = [
    path('admin/', admin.site.urls),

    # API URL Endpoints for Sales microservice for:
    # Sales Representatives
    path('salespeople/<int:pk>/', api_show_rep, name="api_show_rep"),
    path('salespeople/', api_list_reps, name="api_list_reps"),
    # Customers
    path('customers/<int:pk>/', api_show_customer, name="api_show_customer"),
    path('customers/', api_list_customers, name="api_list_customers"),
    # Sale Records
    path('sales/<int:pk>/', api_show_sale, name="api_show_sale"),
    path('sales/', api_list_sales, name="api_list_sales"),
    # AutomobileVO
    path("automobiles/", api_list_autos, name="api_list_autos"),
]
