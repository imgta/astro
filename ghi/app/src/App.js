import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import RepForm from './Sales/RepForm';
import RepList from './Sales/RepList';
import CustomerList from './Sales/CustomerList';
import CustomerForm from './Sales/CustomerForm';
import SaleList from './Sales/SalesList';
import SaleForm from './Sales/SalesForm';
import SaleHistory from './Sales/RepSalesHistory';
import TechnicianForm from './Services/TechnicianForm';
import AppointmentForm from './Services/AppointmentForm';
import AppointmentList from './Services/AppointmentList';
import AppointmentHistory from './Services/AppointmentHistory';
import TechnicianList from './Services/TechnicianList';
import ModelForm from './Inventory/ModelForms';
import ModelList from './Inventory/ModelList';
import AutomobileList from './Inventory/AutomobileList';
import AutomobileForm from './Inventory/AutomobileForm';
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import './styles/global.css';
import CursorLine from './CursorLine';

function App() {
  return (
    <BrowserRouter>
      <CursorLine />
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path='salespeople'>
            <Route path='' element={ <RepList/> }/>
            <Route path='new' element={<RepForm />}/>
          </Route>
          <Route path='customers'>
            <Route path='' element={ <CustomerList/> }/>
            <Route path='new' element={<CustomerForm />}/>
          </Route>
          <Route path='sales'>
            <Route path='' element={ <SaleList/> }/>
            <Route path='new' element={<SaleForm />}/>
            <Route path='history' element={<SaleHistory />}/>
          </Route>
          <Route path="technicians">
            <Route path="" element={ <TechnicianList/> }/>
            <Route path="new" element={ <TechnicianForm/> }/>
          </Route>
          <Route path="appointments">
            <Route path="" element={ <AppointmentList/> }/>
            <Route path="history" element={ <AppointmentHistory/> }/>
            <Route path="new" element={ <AppointmentForm/> }/>
          </Route>
          <Route path="models">
            <Route path="" element={ <ModelList/> }/>
            <Route path="new" element={ <ModelForm/> }/>
          </Route>
          <Route path="automobiles">
            <Route path="" element={ <AutomobileList/>}/>
            <Route path="new" element={ <AutomobileForm/> }/>
          </Route>
          <Route path="manufacturers">
            <Route path="" element={ <ManufacturerList/>}/>
            <Route path="new" element={ <ManufacturerForm/> }/>
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
