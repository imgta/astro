// import { NavLink } from 'react-router-dom';
// import profileIcon from './images/profileIcon.png';
// import profileIcon2 from './images/profileIcon2.png';
// import './Nav.css';

// function Nav() {
//   return (
//     <nav className="navbar navbar-expand-lg navbar-dark custom-navbar">
//       <div className="container-fluid justify-content-center">
//         <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
//           <span className="navbar-toggler-icon"></span>
//         </button>
//         <div className="collapse navbar-collapse" id="navbarSupportedContent">
//           <ul className="navbar-nav mb-2 mb-lg-0">
//             <li className="nav-item">
//               <NavLink id="navTitle" className="navbar-brand centered" to="/">astro</NavLink>
//             </li>
//             <li className="nav-divider"></li>
//             <li className="nav-item dropdown">
//               <NavLink className="nav-link dropdown-toggle" to="#" id="salesDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
//                 Sales
//               </NavLink>
//               <ul className="dropdown-menu" aria-labelledby="salesDropdown">
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/sales">View Sales</NavLink>
//                 </li>
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/salespeople">View Salespeople</NavLink>
//                 </li>
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/sales/history">View Salesperson History</NavLink>
//                 </li>
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/customers">View Customers</NavLink>
//                 </li>
//               </ul>
//             </li>
//             <li className="nav-item dropdown">
//               <NavLink className="nav-link dropdown-toggle" to="#" id="inventoryDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
//                 Inventory
//               </NavLink>
//               <ul className="dropdown-menu" aria-labelledby="inventoryDropdown">
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/manufacturers">View Manufacturers</NavLink>
//                 </li>
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/models">View Models</NavLink>
//                 </li>
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/automobiles">View Ships</NavLink>
//                 </li>
//               </ul>
//             </li>
//             <li className="nav-item dropdown">
//               <NavLink className="nav-link dropdown-toggle" to="#" id="serviceDropdown" role="button" data-bs-toggle="dropdown" aria-expanded="false">
//                 Services
//               </NavLink>
//               <ul className="dropdown-menu" aria-labelledby="serviceDropdown">
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/technicians">View Technicians</NavLink>
//                 </li>
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/appointments">View Appointments</NavLink>
//                 </li>
//                 <li>
//                   <NavLink className="dropdown-item" end  to="/appointments/history">View Service History</NavLink>
//                 </li>
//               </ul>
//             </li>
//           </ul>
//         </div>
//         <div className="navbar-nav ml-auto">
//           <NavLink className="nav-link" to="/profile">
//             <img id="accountIcon" src={profileIcon2} alt="Profile Icon 2" />
//             <img id="hoverIcon" src={profileIcon} alt="Profile Icon" />
//           </NavLink>
//         </div>
//       </div>
//     </nav>
//   );
// }

// export default Nav;
