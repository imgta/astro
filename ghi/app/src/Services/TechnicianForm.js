import React, { useState } from 'react';

function TechnicianForm({ handleFormSubmit }) {
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [employeeID, setEmployeeID] = useState('');

  const handleFirstNameChange = (event) => {
    const value = event.target.value;
    setFirstName(value);
  };

  const handleLastNameChange = (event) => {
    const value = event.target.value;
    setLastName(value);
  };

  const handleEmployeeIDChange = (event) => {
    const value = event.target.value;
    setEmployeeID(value);
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      first_name: firstName,
      last_name: lastName,
      employee_id: employeeID,
    };

    try {
      const response = await fetch('http://localhost:8080/api/technicians/', {
        method: 'POST',
        body: JSON.stringify(data),
        headers: {
          'Content-Type': 'application/json',
        },
      });

      if (response.ok) {
        setFirstName('');
        setLastName('');
        setEmployeeID('');
        handleFormSubmit(); // Invoke the callback function from TechnicianList
      } else {
        console.error('Failed to submit technician form');
      }
    } catch (error) {
      console.error('Error while submitting technician form', error);
    }
  };

  return (
    <div className="form-container embedform">
      <form onSubmit={handleSubmit} id="create-technician-form">
        <div className="row mb-3">
          <div className="col">
            <div className="form-floating mb-3">
              <input
                onChange={handleFirstNameChange}
                required
                placeholder="First name"
                type="text"
                id="firstName"
                name="firstName"
                className="form-control"
                value={firstName}
              />
              <label htmlFor="firstName">First Name</label>
            </div>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <div className="form-floating mb-3">
              <input
                onChange={handleLastNameChange}
                required
                placeholder="Last name"
                type="text"
                id="lastName"
                name="lastName"
                className="form-control"
                value={lastName}
              />
              <label htmlFor="lastName">Last Name</label>
            </div>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <div className="form-floating mb-3">
              <input
                onChange={handleEmployeeIDChange}
                required
                placeholder="Employee ID"
                type="text"
                id="employeeID"
                name="employeeID"
                className="form-control"
                value={employeeID}
              />
              <label htmlFor="employeeID">Employee ID</label>
            </div>
          </div>
        </div>
        <div className="text-center">
          <button type="submit" className="btn btn-outline-primary btn-lg btn-block create-button">
            Create
          </button>
        </div>
      </form>
    </div>
  );
}

export default TechnicianForm;
