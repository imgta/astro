import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {useEffect, useState} from 'react';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

async function loadData(){
    const response = await fetch('http://localhost:8080/api/appointments/');
    if(response.ok){
        const apptData = await response.json();
        return apptData;
    } else {
        console.error('Failed to fetch appointment data');
        return null;
    }
}

export default function AppointmentHistory() {
    const [apptData, setApptData] = useState(null);
    const [searchTerm, setSearchTerm] = useState('');

    useEffect(() => {
        loadData().then(data => {
            setApptData(data);
        });
    }, []);

    const handleSearch = (event) => {
        setSearchTerm(event.target.value);
    };

    if(!apptData || !apptData.appointments){
        return null;
    }
    const filteredAppointments = apptData.appointments.filter((appt) =>
        appt.vin.toLowerCase().includes(searchTerm.toLowerCase())
    );

    return (
        <div>
            <div id="tableTitleAlign">
                <h3 id="tableTitle">Service History</h3>
            </div>
            <div>
                <input
                    type="text"
                    placeholder="Search by VIN..."
                     value={searchTerm}
                    onChange={handleSearch}
                    className="form-control mb-3"
                    style={{ width: "850px" }}
                />
            </div>
            <TableContainer component={Paper} id="table">
            <Table sx={{ minWidth: 700 }} aria-label="customized table">
                <TableHead>
                <TableRow>
                    <StyledTableCell align="center">VIN</StyledTableCell>
                    <StyledTableCell align="center">VIP</StyledTableCell>
                    <StyledTableCell align="center">Customer</StyledTableCell>
                    <StyledTableCell align="center">Date</StyledTableCell>
                    <StyledTableCell align="center">Time</StyledTableCell>
                    <StyledTableCell align="center">Technician</StyledTableCell>
                    <StyledTableCell align="center">Reason</StyledTableCell>
                    <StyledTableCell align="center">Status</StyledTableCell>
                </TableRow>
                </TableHead>
                <TableBody>
                    {filteredAppointments.map((appt) => {
                        const dateTime = new Date(appt.date_time);
                        const date = dateTime.toLocaleDateString('en-US', {
                            month: '2-digit',
                            day: '2-digit',
                            year: 'numeric',
                        });
                        const time = dateTime.toLocaleTimeString('en-US', {
                            hour: '2-digit',
                            minute: '2-digit',
                            hour12: true,
                        });
                        return (
                            <StyledTableRow key={appt.id}>
                            <StyledTableCell align="center">{appt.vin}</StyledTableCell>
                            <StyledTableCell align="center">{String(appt.is_vip).charAt(0).toUpperCase() + String(appt.is_vip).slice(1)}</StyledTableCell>
                            <StyledTableCell align="center">{appt.customer}</StyledTableCell>
                            <StyledTableCell align="center">{date}</StyledTableCell>
                            <StyledTableCell align="center">{time}</StyledTableCell>
                            <StyledTableCell align="center">{appt.technician.first_name} {appt.technician.last_name}</StyledTableCell>
                            <StyledTableCell align="center">{appt.reason}</StyledTableCell>
                            <StyledTableCell align="center">{appt.status}</StyledTableCell>
                            </StyledTableRow>
                        );
                    })}
                </TableBody>
            </Table>
            </TableContainer>
        </div>
    );
}

// import React, {useState, useEffect} from 'react'


// async function loadData(){
//     const response = await fetch('http://localhost:8080/api/appointments/');
//     if(response.ok){
//         const apptData = await response.json();
//         return apptData;
//     } else {
//         console.error('Failed to fetch appointment data');
//         return null;
//     }
// }

// function AppointmentHistory(){
//     const [apptData, setApptData] = useState(null);
//     const [searchTerm, setSearchTerm] = useState('');

//     useEffect(() => {
//         loadData().then(data => {
//             setApptData(data);
//         });
//     }, []);

//     const handleSearch = (event) => {
//         setSearchTerm(event.target.value);
//     };

//     if(!apptData || !apptData.appointments){
//         return null;
//     }
//     const filteredAppointments = apptData.appointments.filter((appt) =>
//         appt.vin.toLowerCase().includes(searchTerm.toLowerCase())
//     );

//     return(
//         <div className="container">
//             <div className="my-5 card">
//             <h3>Service History</h3>
//                 <input
//                     type="text"
//                     placeholder="Search by VIN..."
//                     value={searchTerm}
//                     onChange={handleSearch}
//                     className="form-control mb-3"
//                 />
//                 <table className="table table-striped">
//                     <thead className="bg-dark text-light">
//                         <tr>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIN</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIP</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Customer</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Date</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Time</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Technician</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Reason</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Status</th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {filteredAppointments.map((appt) => {
//                             const dateTime = new Date(appt.date_time);
//                             const date = dateTime.toLocaleDateString('en-US', {
//                                 month: '2-digit',
//                                 day: '2-digit',
//                                 year: 'numeric',
//                             });
//                             const time = dateTime.toLocaleTimeString('en-US', {
//                                 hour: '2-digit',
//                                 minute: '2-digit',
//                                 hour12: true,
//                             });
//                             return (
//                                 <tr key={appt.id}>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.vin}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{String(appt.is_vip).charAt(0).toUpperCase() + String(appt.is_vip).slice(1)}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.customer}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{date}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{time}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.technician.first_name} {appt.technician.last_name}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.reason}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.status}</td>
//                                 </tr>
//                             );
//                         })}
//                     </tbody>
//                 </table>
//             </div>
//         </div>
//     )
// }


// export default AppointmentHistory;
