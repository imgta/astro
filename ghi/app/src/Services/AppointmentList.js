import React, { useState, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Fab, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import AppointmentForm from './AppointmentForm';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import '../styles/global.css';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

async function loadData() {
  const response = await fetch('http://localhost:8080/api/appointments/');
  if (response.ok) {
    const apptData = await response.json();
    apptData.appointments = apptData.appointments.filter((appt) => appt.status === 'CREATED');
    return apptData;
  } else {
    console.error('Failed to fetch appointment data');
    return null;
  }
}

async function checkVIPStatus(appt) {
  const response = await fetch('http://localhost:8100/api/automobiles/');
  if (response.ok) {
    const autoData = await response.json();
    const vipAppts = autoData.autos.filter((auto) => auto.vin === appt.vin && auto.sold);
    appt.is_vip = vipAppts.length > 0;
  } else {
    console.error('Failed to fetch auto data');
  }
  return appt;
}

export default function AppointmentList() {
  const [apptData, setApptData] = useState(null);
  const [refresh, setRefresh] = useState(false);
  const [open, setOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 10; // Adjust the number of items per page as needed

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFormSubmit = async () => {
    await loadData();
    setRefresh(!refresh);
  };

  useEffect(() => {
    loadData().then(async (data) => {
      if (data.appointments) {
        const appointments = await Promise.all(data.appointments.map(checkVIPStatus));
        setApptData({ appointments });
      }
    });
  }, [refresh]);

  if (!apptData || !apptData.appointments) {
    return null;
  }

  async function cancel(id) {
    try {
      const url = `http://localhost:8080/api/appointments/${id}/cancel/`;
      const fetchConfig = {
        method: 'put',
      };
      const response = await fetch(url, fetchConfig);
      setRefresh(!refresh);
    } catch (e) {
      console.error('Failed to cancel appointment');
    }
  }

  async function complete(id) {
    try {
      const url = `http://localhost:8080/api/appointments/${id}/finish/`;
      const fetchConfig = {
        method: 'put'
      };
      const response = await fetch(url, fetchConfig);
      setRefresh(!refresh);
    } catch (e) {
      console.error('Failed to complete appointment');
    }
  }

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const start = currentPage * itemsPerPage;
  const end = start + itemsPerPage;
  const currentAppointments = apptData.appointments.slice(start, end);
  const numPages = Math.ceil(apptData.appointments.length / itemsPerPage);

  return (
    <div>
      <div id="tableTitleAlign">
        <h3 id="tableTitle">Appointments</h3>
      </div>
      <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">VIN</StyledTableCell>
              <StyledTableCell align="center">VIP</StyledTableCell>
              <StyledTableCell align="center">Customer</StyledTableCell>
              <StyledTableCell align="center">Date</StyledTableCell>
              <StyledTableCell align="center">Time</StyledTableCell>
              <StyledTableCell align="center">Technician</StyledTableCell>
              <StyledTableCell align="center">Reason</StyledTableCell>
              <StyledTableCell align="center">Status</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {currentAppointments.map((appt) => {
              const dateTime = new Date(appt.date_time);
              const date = dateTime.toLocaleDateString('en-US', {
                month: '2-digit',
                day: '2-digit',
                year: 'numeric'
              });
              const time = dateTime.toLocaleTimeString('en-US', {
                hour: '2-digit',
                minute: '2-digit',
                hour12: true
              });

              return (
                <StyledTableRow key={appt.id}>
                  <StyledTableCell component="th" scope="row">
                    {appt.vin}
                  </StyledTableCell>
                  <StyledTableCell align="center">
                    {String(appt.is_vip).charAt(0).toUpperCase() + String(appt.is_vip).slice(1)}
                  </StyledTableCell>
                  <StyledTableCell align="center">{appt.customer}</StyledTableCell>
                  <StyledTableCell align="center">{date}</StyledTableCell>
                  <StyledTableCell align="center">{time}</StyledTableCell>
                  <StyledTableCell align="center">{appt.technician.first_name} {appt.technician.last_name}</StyledTableCell>
                  <StyledTableCell align="center">{appt.reason}</StyledTableCell>
                  <StyledTableCell align="center">

                    <button className="btn btn-outline-success btn-sm" onClick={() => complete(appt.id)}>
                      Complete
                    </button>
                    &nbsp;&nbsp;
                    <button className="btn btn-outline-danger btn-sm" onClick={() => cancel(appt.id)}>
                      Cancel
                    </button>
                  </StyledTableCell>
                </StyledTableRow>
              );
            })}
          </TableBody>
        </Table>
      </TableContainer>

      {numPages > 1 && (
        <div>
          <Button color="secondary" disabled={currentPage === 0} onClick={() => handlePageChange(currentPage - 1)}>
            Previous
          </Button>
          <Button color="secondary" disabled={currentPage === numPages - 1} onClick={() => handlePageChange(currentPage + 1)}>
            Next
          </Button>
        </div>
      )}

      <div className='fab-container'>
        <Fab color="secondary" aria-label="add" onClick={handleClickOpen}>
          <AddIcon />
        </Fab>
        <Dialog open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
          <DialogTitle>Add New Appointment</DialogTitle>
          <AppointmentForm handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
        </Dialog>
      </div>
    </div>
  );
}

// import React, {useState, useEffect} from 'react'


// async function loadData(){
//     const response = await fetch('http://localhost:8080/api/appointments/');
//     if(response.ok){
//         const apptData = await response.json();
//         apptData.appointments = apptData.appointments.filter(appt => appt.status === 'CREATED');
//         return apptData;
//     } else {
//         console.error('Failed to fetch appointment data');
//         return null;
//     }
// }


// async function checkVIPStatus(appt) {
//     const response = await fetch('http://localhost:8100/api/automobiles/');
//     if (response.ok) {
//         const autoData = await response.json();
//         const vipAppts = autoData.autos.filter((auto) =>
//             auto.vin === appt.vin && auto.sold
//         );
//         appt.is_vip = vipAppts.length > 0;
//     } else {
//         console.error('Failed to fetch auto data');
//     }
//     return appt;
// }


// function AppointmentList(){
//     const [apptData, setApptData] = useState(null);
//     const [refresh, setRefresh] = useState(false);

//     useEffect(() => {
//         loadData().then(async (data) => {
//             if (data.appointments) {
//                 const appointments = await Promise.all(
//                     data.appointments.map(checkVIPStatus)
//                 );
//             setApptData({appointments });
//             }
//         });
//     }, [refresh]);

//     if(!apptData || !apptData.appointments){
//         return null;
//     }

//     async function cancel(id){
//         try{
//             const url = `http://localhost:8080/api/appointments/${id}/cancel/`
//             const fetchConfig = {
//                 method: 'put'
//             };
//             const response = await fetch(url, fetchConfig)
//             setRefresh(!refresh);
//         }catch (e) {

//         }
//     }

//     async function complete(id){
//         try{
//             const url = `http://localhost:8080/api/appointments/${id}/finish/`
//             const fetchConfig = {
//                 method: 'put'
//             }
//             const response = await fetch(url, fetchConfig)
//             setRefresh(!refresh);
//         }catch (e){

//         }
//     }

//     return(
//         <div className="container">
//             <div className="my-5 card">
//             <h3>Appointments</h3>
//                 <table className="table table-striped">
//                     <thead className="bg-dark text-light">
//                         <tr>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIN</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">VIP</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Customer</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Date</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Time</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Technician</th>
//                             <th style={{ textAlign: 'center', verticalAlign: 'middle' }} scope="col">Reason</th>
//                             <th></th>
//                             <th></th>
//                         </tr>
//                     </thead>
//                     <tbody>
//                         {apptData.appointments.map((appt) => {
//                             const dateTime = new Date(appt.date_time);
//                             const date = dateTime.toLocaleDateString('en-US', {
//                                 month: '2-digit',
//                                 day: '2-digit',
//                                 year: 'numeric'
//                             });
//                             const time = dateTime.toLocaleTimeString('en-US', {
//                                 hour: '2-digit',
//                                 minute: '2-digit',
//                                 hour12: true
//                             })
//                             return (
//                                 <tr key={appt.id}>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.vin}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{String(appt.is_vip).charAt(0).toUpperCase() + String(appt.is_vip).slice(1)}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.customer}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{date}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{time}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.technician.first_name} {appt.technician.last_name}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>{appt.reason}</td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
//                                         <button className="btn btn-danger" onClick={() => {cancel(appt.id)}}>
//                                             Cancel
//                                         </button>
//                                     </td>
//                                     <td style={{ textAlign: 'center', verticalAlign: 'middle' }}>
//                                         <button className="btn btn-success" onClick={() => {complete(appt.id)}}>
//                                             Complete
//                                         </button>
//                                     </td>
//                                 </tr>
//                             )
//                         })}
//                     </tbody>
//                 </table>
//             </div>
//         </div>
//     )

// }

// export default AppointmentList;
