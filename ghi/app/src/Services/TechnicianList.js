import React, { useEffect, useState } from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Fab, Button, Dialog, DialogTitle } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import TechnicianForm from './TechnicianForm';
import '../styles/global.css';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

const itemsPerPage = 5;

export default function TechnicianList() {
  const [techData, setTechData] = useState([]);
  const [open, setOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const numPages = Math.ceil(techData.length / itemsPerPage);

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFormSubmit = async () => {
    await loadData();
  };

  async function loadData() {
    try {
      const response = await fetch('http://localhost:8080/api/technicians/');
      if (response.ok) {
        const techData = await response.json();
        setTechData(techData.technicians);
      } else {
        console.error('Failed to fetch technician data');
      }
    } catch (error) {
      console.error('Error while fetching technician data', error);
    }
  }

  useEffect(() => {
    loadData();
  }, []);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderTableRows = () => {
    const start = currentPage * itemsPerPage;
    const end = start + itemsPerPage;
    const pageData = techData.slice(start, end);

    return pageData.map((tech) => (
      <StyledTableRow key={tech.employee_id}>
        <StyledTableCell align="center">{tech.employee_id}</StyledTableCell>
        <StyledTableCell align="center">{tech.first_name}</StyledTableCell>
        <StyledTableCell align="center">{tech.last_name}</StyledTableCell>
      </StyledTableRow>
    ));
  };

  return (
    <div>
      <div id="tableTitleAlign">
        <h3 id="tableTitle">Technicians</h3>
      </div>
      <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Employee ID</StyledTableCell>
              <StyledTableCell align="center">First Name</StyledTableCell>
              <StyledTableCell align="center">Last Name</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>{renderTableRows()}</TableBody>
        </Table>
      </TableContainer>

      {numPages > 1 && (
        <div>
          <Button color="secondary" disabled={currentPage === 0} onClick={() => handlePageChange(currentPage - 1)}>
            Previous
          </Button>
          <Button
            color="secondary"
            disabled={currentPage === numPages - 1}
            onClick={() => handlePageChange(currentPage + 1)}
          >
            Next
          </Button>
        </div>
      )}

      <div className="fab-container">
        <Fab color="secondary" aria-label="add" onClick={handleClickOpen}>
          <AddIcon />
        </Fab>
        <Dialog open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
          <DialogTitle>Add New Technician</DialogTitle>
          <TechnicianForm handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
        </Dialog>
      </div>
    </div>
  );
}
