import React, { useState, useEffect } from 'react';

function AppointmentForm({ handleFormSubmit }) {
  const [dateTime, setDateTime] = useState('');
  const [reason, setReason] = useState('');
  const [vin, setVin] = useState('');
  const [customer, setCustomer] = useState('');
  const [technician, setTechnician] = useState('');
  const [technicians, setTechnicians] = useState([]);

  const handleDateTimeChange = (event) => {
    const value = event.target.value;
    setDateTime(value);
  };

  const handleReasonChange = (event) => {
    const value = event.target.value;
    setReason(value);
  };

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleCustomerChange = (event) => {
    const value = event.target.value;
    setCustomer(value);
  };

  const handleTechnicianChange = (event) => {
    const value = event.target.value;
    setTechnician(value);
  };

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/technicians/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  };

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {
      date_time: dateTime,
      reason: reason,
      vin: vin,
      customer: customer,
      technician: technician,
    };

    const apptUrl = 'http://localhost:8080/api/appointments/';
    const fetchConfig = {
      method: 'post',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(apptUrl, fetchConfig);

    if (response.ok) {
      setDateTime('');
      setReason('');
      setVin('');
      setCustomer('');
      setTechnician('');
      handleFormSubmit(); // Call the callback function to trigger the update
    }
  };

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="form-container embedform">
      <form onSubmit={handleSubmit} id="create-appt-form">
        <div className="row mb-3">
          <div className="col" style={{ margin: '0' }}>
            <select
              onChange={handleTechnicianChange}
              name="technician"
              id="technician"
              required
              value={technician}
              className="form-select mb-3 select-field "
              style={{ width: '225px' }}
            >
              <option value="">Choose a Technician</option>
              {technicians.map((technician) => (
                <option
                  key={technician.employee_id}
                  value={technician.employee_id}
                >
                  {technician.first_name} {technician.last_name}
                </option>
              ))}
            </select>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <div className="form-floating mb-3">
              <input
                onChange={handleVinChange}
                required
                placeholder="VIN"
                type="text"
                id="vin"
                name="vin"
                className="form-control"
                minLength="17"
                maxLength="17"
                value={vin}
              />
              <label htmlFor="vin">Automobile VIN</label>
            </div>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <div className="form-floating mb-3">
              <input
                onChange={handleCustomerChange}
                required
                placeholder="Customer"
                type="text"
                id="customer"
                name="customer"
                className="form-control"
                value={customer}
              />
              <label htmlFor="customer">Customer</label>
            </div>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <div className="form-floating mb-3">
              <input
                onChange={handleDateTimeChange}
                required
                type="datetime-local"
                id="dateTime"
                name="dateTime"
                className="form-control"
                value={dateTime}
              />
              <label htmlFor="dateTime">Date Time</label>
            </div>
          </div>
        </div>
        <div className="row mb-3">
          <div className="col">
            <div className="form-floating mb-3">
              <input
                onChange={handleReasonChange}
                required
                placeholder="Reason"
                type="text"
                id="reason"
                name="reason"
                className="form-control"
                value={reason}
              />
              <label htmlFor="reason">Reason</label>
            </div>
          </div>
        </div>
        <div className="text-center">
          <button
            type="submit"
            className="btn btn-outline-primary btn-lg btn-block create-button"
          >
            Create
          </button>
        </div>
      </form>
    </div>
  );
}

export default AppointmentForm;
