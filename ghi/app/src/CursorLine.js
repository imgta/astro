import React, { useRef, useEffect } from 'react';
import './styles/CursorLine.css';

const CursorLine = () => {
    const cursorRef = useRef(null);
    const horizontalRef = useRef(null);
    const topLeftRef = useRef(null);
    const topRightRef = useRef(null);
    const bottomLeftRef = useRef(null);
    const bottomRightRef = useRef(null);

    useEffect(() => {
        const handleMouseMove = (event) => {
            const { clientX, clientY } = event;
            const cursorLine = cursorRef.current;
            const horizontalLine = horizontalRef.current;
            const topLeft = topLeftRef.current;
            const topRight = topRightRef.current;
            const bottomLeft = bottomLeftRef.current;
            const bottomRight = bottomRightRef.current;

            const dotTopLeft = document.querySelector('.cursor-dot-top-left');
            const dotTopRight = document.querySelector('.cursor-dot-top-right');
            const dotBotLeft = document.querySelector('.cursor-dot-bottom-left');

            cursorLine.style.left = `${clientX}px`;
            horizontalLine.style.top = `${clientY}px`;
            topLeft.style.left = `${clientX - 5}px`;
            topLeft.style.top = `${clientY - 5}px`;
            topRight.style.left = `${clientX - 5}px`;
            topRight.style.top = `${clientY - 5}px`;
            bottomLeft.style.left = `${clientX - 5}px`;
            bottomLeft.style.top = `${clientY - 5}px`;
            bottomRight.style.left = `${clientX - 5}px`;
            bottomRight.style.top = `${clientY - 5}px`;

            const linkElements = document.querySelectorAll('a');
            const buttonElements = document.querySelectorAll('button');
            const isHoveringLink = Array.from(linkElements).some(link => link.matches(':hover'));
            const isHoveringButton = Array.from(buttonElements).some(button => button.matches(':hover'));

            if (isHoveringLink || isHoveringButton) {
                cursorLine.classList.add('active');
                horizontalLine.classList.add('active');
                topLeft.classList.add('inactive');
                topRight.classList.add('inactive');
                bottomLeft.classList.add('inactive');
                bottomRight.classList.add('inactive');

                dotTopLeft.classList.add('shadow-none');
                dotTopRight.classList.add('shadow-none');
                dotBotLeft.classList.add('shadow-none');

            } else {
                cursorLine.classList.remove('active');
                horizontalLine.classList.remove('active');
                topLeft.classList.remove('inactive');
                topRight.classList.remove('inactive');
                bottomLeft.classList.remove('inactive');
                bottomRight.classList.remove('inactive');

                dotTopLeft.classList.remove('shadow-none');
                dotTopRight.classList.remove('shadow-none');
                dotBotLeft.classList.remove('shadow-none');
            }
        };

        document.addEventListener('mousemove', handleMouseMove);

        return () => {
            document.removeEventListener('mousemove', handleMouseMove);
        };
    }, []);

    return (
        <>
            <div ref={cursorRef} className="cursor-line cursor-line-vertical"></div>
            <div ref={horizontalRef} className="cursor-line cursor-line-horizontal"></div>
            <div ref={topLeftRef} className="cursor-dot cursor-dot-top-left"></div>
            <div ref={topRightRef} className="cursor-dot cursor-dot-top-right"></div>
            <div ref={bottomLeftRef} className="cursor-dot cursor-dot-bottom-left"></div>
            <div ref={bottomRightRef} className="cursor-dot cursor-dot-bottom-right"></div>
        </>
    );
};

export default CursorLine;
