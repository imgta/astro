import React, { useEffect } from 'react';
import { NavLink } from 'react-router-dom';
import classNames from 'classnames';
import * as NavigationMenu from '@radix-ui/react-navigation-menu';
import { CaretDownIcon } from '@radix-ui/react-icons';
import './styles/navbar.css';
import './styles/naveffect.css';
import logo from './images/logoastro_icon.png';
import astro from './images/astrogt.png';



function Nav() {

    const ListItem = React.forwardRef(({ className, children, title, ...props }, forwardedRef) => (
        <li>
        <NavigationMenu.Link asChild>
            <a className={classNames('ListItemLink', className)} {...props} ref={forwardedRef}>
            <div className="ListItemHeading">{title}</div>
            <p className="ListItemText">{children}</p>
            </a>
        </NavigationMenu.Link>
        </li>
    ));

    return (
    <NavigationMenu.Root className="NavigationMenuRoot">
        <NavLink to="/">
            <img src={logo} alt="Logo" className="logo" />
        </NavLink>
        <NavigationMenu.List className="NavigationMenuList">
            <NavigationMenu.Item>
            <NavigationMenu.Trigger className="NavigationMenuTrigger" style={{fontFamily:"'Silkscreen', cursive"}}>
                DISCOVER
                {/* <CaretDownIcon className="CaretDown" aria-hidden /> */}
            </NavigationMenu.Trigger>
            <NavigationMenu.Content className="NavigationMenuContent">
            <ul className="List one">
                <li style={{ gridRow: "span 3"}}>
                    <NavigationMenu.Link asChild>
                    <a className="Callout" href="/">
                        <img src={astro} alt="" width="80%" style={{margin:"-9% 35%"}} />
                    <div className="CalloutHeading" style={{fontFamily:"'Silkscreen', cursive"}}>
                    ASTRO.io</div>
                    <div className="tagline">
                    <p>Intergalactic exploration!</p>
                    </div>
                    </a>
                </NavigationMenu.Link>
                </li>

            <li className="nav-list-item">
                <p style={{fontFamily:"'Cascadia Code', truetype", fontSize: "14px", color:"#fff"}}>
                    UPGRADE:</p>
                <div className="info-menu">
                    <iconify-icon icon="fluent:brain-circuit-20-filled" width="25" style={{color:"rgba(225,225,225,0.4)"}}/>
                    <p className="CalloutText" style={{fontFamily:"'Cascadia Code', truetype", fontSize:"12px", marginLeft:"5px", marginTop:"3px"}}>
                        Find the latest firmware upgrades!</p>
                </div>
            </li>

            <li className="nav-list-item">
                <p style={{fontFamily:"'Cascadia Code', truetype",fontSize:"14px",color:"#fff" }}>
                    DETOX:</p>
                <div className="info-menu">
                    <iconify-icon icon="healthicons:hazardous" width="25" style={{color:"rgba(225,225,225,0.4)"}}/>
                    <p className="CalloutText" style={{fontFamily:"'Cascadia Code', truetype", fontSize:"12px", marginLeft:"5px", marginTop:"3px"}}>
                        Propulsion gel starting to smell?</p>
                </div>
            </li>

            <li className="nav-list-item">
                <p style={{fontFamily:"'Cascadia Code', truetype", fontSize: "14px", color:"#fff"}}>
                    REPAIR:</p>
                <div className="info-menu">
                    <iconify-icon icon="game-icons:auto-repair" width="25" style={{color:"rgba(225,225,225,0.4)"}}/>
                    <p className="CalloutText" style={{fontFamily:"'Cascadia Code', truetype", fontSize:"12px", marginLeft:"5px", marginTop:"3px"}}>
                        Our nanobots are non-invasive!</p>
                </div>
            </li>

            </ul>
            </NavigationMenu.Content>
        </NavigationMenu.Item>
        <svg
            class="line-4"
            width="34"
            height="34"
            viewBox="0 0 39 39"
            fill="none"
            xmlns="http://www.w3.org/2000/svg"
        >
            <path d="M10.5718 38.1117L27.5359 0.00123846" stroke="white" />
        </svg>
        <NavigationMenu.Item>
            <NavigationMenu.Trigger className="NavigationMenuTrigger" style={{fontFamily:"'Silkscreen', cursive"}}>
            LAUNCH
            {/* <CaretDownIcon className="CaretDown" aria-hidden /> */}
            </NavigationMenu.Trigger>
            <NavigationMenu.Content className="NavigationMenuContent">
                <ul className="List two">

                <li>
                <div className="ListHeading">
                    <iconify-icon icon="icon-park-outline:sales-report" width="20" className="icon" />
                    <p className="ListItemHeading"> SALES</p><br/>
                </div>

                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/customers"><span>Customers</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/salespeople"><span>Sales Reps</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/sales"><span>Sales History</span></NavLink>
                    </nav>
                </li>

                <li>
                <div className="ListHeading">
                    <iconify-icon icon="game-icons:spaceship" width="20"/>
                    <p className="ListItemHeading"> INVENTORY</p><br/>
                </div>

                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/automobiles"><span>Vehicles</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/models"><span>Models</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/manufacturers"><span>Manufacturers</span></NavLink>
                    </nav>

                </li>

                <li>
            <div className="ListHeading">
                <iconify-icon icon="fluent:bot-sparkle-24-regular" width="20"/>
                <p className="ListItemHeading"> SERVICES</p><br/>
            </div>
                <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/technicians"><span>Technicians</span></NavLink>
                </nav>
                <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/appointments"><span>Appointments</span></NavLink>
                </nav>
                <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/appointments/history"><span>Repair History</span></NavLink>
                </nav>

                </li>

                </ul>
            </NavigationMenu.Content>
        </NavigationMenu.Item>

        {/* <NavigationMenu.Item>
            <NavigationMenu.Trigger className="NavigationMenuTrigger">
            Sales <CaretDownIcon className="CaretDown" aria-hidden />
            </NavigationMenu.Trigger>
            <NavigationMenu.Content className="NavigationMenuContent">
                <ul className="List three">
                <li title="Sales">
                    <iconify-icon icon="icon-park-outline:sales-report" width="30" className="icon" />

                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/customers"><span>Customers</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/salespeople"><span>Sales Reps</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/sales"><span>Sales History</span></NavLink>
                    </nav>
                </li>
                </ul>
            </NavigationMenu.Content>
        </NavigationMenu.Item>

        <NavigationMenu.Item>
            <NavigationMenu.Trigger className="NavigationMenuTrigger">
            Inventory <CaretDownIcon className="CaretDown" aria-hidden />
            </NavigationMenu.Trigger>
            <NavigationMenu.Content className="NavigationMenuContent">
                <ul className="List three">
                <li title="Inventory">
                <iconify-icon icon="game-icons:spaceship" width="30" className="icon"/>
                    <br/>

                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/automobiles"><span>Vehicles</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/models"><span>Models</span></NavLink>
                    </nav>
                    <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/manufacturers"><span>Manufacturers</span></NavLink>
                    </nav>

                </li>
                </ul>
            </NavigationMenu.Content>
        </NavigationMenu.Item>

        <NavigationMenu.Item>
            <NavigationMenu.Trigger className="NavigationMenuTrigger">
            Services <CaretDownIcon className="CaretDown" aria-hidden />
            </NavigationMenu.Trigger>
            <NavigationMenu.Content className="NavigationMenuContent">
                <ul className="List three">
                <li title="Services">
                <iconify-icon icon="fluent:bot-sparkle-24-regular" width="30" className="icon"/>

                <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/technicians"><span>Technicians</span></NavLink>
                </nav>
                <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/appointments"><span>Appointments</span></NavLink>
                </nav>
                <nav className="link-effect" id="link-effect">
                    <NavLink className="ListItemText" to="/appointments/history"><span>Repair History</span></NavLink>
                </nav>

                </li>
                </ul>
            </NavigationMenu.Content>
            </NavigationMenu.Item> */}

            <NavigationMenu.Indicator className="NavigationMenuIndicator">
            <div className="Arrow" />
            </NavigationMenu.Indicator>
        </NavigationMenu.List>

        <div className="ViewportPosition">
            <NavigationMenu.Viewport className="NavigationMenuViewport" />
        </div>
        </NavigationMenu.Root>
    );
};

export default Nav;
