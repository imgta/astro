import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {useEffect, useState} from 'react';
import RepForm from './RepForm';
import AddIcon from '@mui/icons-material/Add';
import { Fab, Button, Dialog, DialogTitle } from '@mui/material';
import '../styles/global.css';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
  '&:hover': {
    backgroundColor: '#16213e',
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.2)', // Add a box shadow on hover
    cursor: 'pointer',
  },
}));

const TablePagination = ({ currentPage, numPages, handlePageChange }) => {
  return (
    <div>
      <Button
        color="secondary"
        disabled={currentPage === 0}
        onClick={() => handlePageChange(currentPage - 1)}
      >
        Previous
      </Button>
      <Button
        color="secondary"
        disabled={currentPage === numPages - 1 || numPages === 0}
        onClick={() => handlePageChange(currentPage + 1)}
      >
        Next
      </Button>
    </div>
  );
};

export default function RepList() {
  const [reps, setReps] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 5;

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
      setOpen(false);
  };
  const handleFormSubmit = () => {
      fetchData();
  };

  const fetchData = async () => {
      try {
          const url = 'http://localhost:8090/api/salespeople';
          const response = await fetch(url);

          if (response.ok) {
              const data = await response.json();
              setReps(data.reps);
          }
      } catch (error) {
          console.error(error);
      }
  };

  useEffect(() => {
      fetchData();
  }, []);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderTableRows = () => {
    const start = currentPage * itemsPerPage;
    const end = start + itemsPerPage;
    const pageData = reps.slice(start, end);

    return pageData.map((rep) => (
      <StyledTableRow key={rep.id}>
        <StyledTableCell align="center">{rep.employee_id}</StyledTableCell>
        <StyledTableCell align="center">{rep.first_name}</StyledTableCell>
        <StyledTableCell align="center">{rep.last_name}</StyledTableCell>
      </StyledTableRow>
    ));
  };


  return (
    <div>
        <div id="tableTitleAlign">
        <h3 id="tableTitle">Sales Representatives</h3>
      </div>
      <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Employee ID</StyledTableCell>
              <StyledTableCell align="center">First Name</StyledTableCell>
              <StyledTableCell align="center">Last Name</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderTableRows()}
          </TableBody>
        </Table>
      </TableContainer>
      <TablePagination currentPage={currentPage} numPages={Math.ceil(reps.length / itemsPerPage)} handlePageChange={handlePageChange} />
        <div className='fab-container'>
          <Fab color="secondary" aria-label="add" onClick={handleClickOpen}>
              <AddIcon />
          </Fab>
          <Dialog open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
              <DialogTitle style={{textAlign: "center", marginTop: "6px", fontFamily:"'Cascadia Code', truetype"}}>Add New Representative</DialogTitle>
              <RepForm handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
          </Dialog>
        </div>
    </div>
  );
}
