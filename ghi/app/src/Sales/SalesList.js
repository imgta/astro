import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {useEffect, useState} from 'react';
import { Fab, Dialog, DialogTitle, Button } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import SalesForm from './SalesForm';
import '../styles/global.css';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
  '&:hover': {
    backgroundColor: '#16213e',
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.2)', // Add a box shadow on hover
    cursor: 'pointer',
  },
}));

const TablePagination = ({ currentPage, numPages, handlePageChange }) => {
  return (
    <div>
      <Button
        color="secondary"
        disabled={currentPage === 0}
        onClick={() => handlePageChange(currentPage - 1)}
      >
        Previous
      </Button>
      <Button
        color="secondary"
        disabled={currentPage === numPages - 1 || numPages === 0}
        onClick={() => handlePageChange(currentPage + 1)}
      >
        Next
      </Button>
    </div>
  );
};

export default function SalesList() {
  const [sales, setSales] = useState([]);
  const [autos, setAutos] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 5;
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
      setOpen(false);
  };
  const handleFormSubmit = () => {
      fetchData();
  };

  const fetchData = async () => {
      try {
          const salesUrl = 'http://localhost:8090/api/sales/';
          const autosUrl = 'http://localhost:8100/api/automobiles/';
          const salesResponse = await fetch(salesUrl);
          const autosResponse = await fetch(autosUrl);

          if (!salesResponse.ok) {
                  throw new Error('Request for sales has failed.');
              } else if (!autosResponse.ok) {
                  throw new Error('Request for automobiles has failed.');
              }
              const salesData = await salesResponse.json();
              const autosData = await autosResponse.json();


          // Filtering out unsold cars:
              // Create new filtered array [soldAutos] containing cars marked sold ("sold": true)
              const soldAutos = autosData.autos.filter(auto => auto.sold);
              // Cars in [sales] that have matching vins to filtered [soldAutos] are included in array
              setSales(salesData.sales.filter(sale => soldAutos.some(auto => auto.vin === sale.automobile)));
              // Set [autos] to [soldAutos]
              setAutos(soldAutos);

      } catch (error) {
          console.error(error);
      }
  };

  useEffect(() => {
      fetchData();
  }, []);

  const autosVin = autos.reduce((map, auto) => {
      map[auto.vin] = auto;
      return map;
  }, {});

  // Delineates large numbers by adding commas
  const numberForm = (val) => {
      return val.toLocaleString();
  };

  const handlePageChange = (newPage) => {
    setCurrentPage(newPage);
  };

  const renderTableRows = () => {
    const startIndex = currentPage * itemsPerPage;
    const endIndex = startIndex + itemsPerPage;
    const slicedSales = sales.slice(startIndex, endIndex);

    return slicedSales.map((sale) => {
      const automobile = autosVin[sale.automobile];

      return (
        <StyledTableRow key={sale.id}>
          <StyledTableCell align="center">{sale.rep}</StyledTableCell>
          <StyledTableCell align="center">{sale.customer}</StyledTableCell>
          <StyledTableCell align="center">{sale.automobile}</StyledTableCell>
          <StyledTableCell align="center">
            {automobile ? `${automobile.model.manufacturer.name}: ${automobile.model.name}` : 'N/A'}
          </StyledTableCell>
          <StyledTableCell align="center">${numberForm(sale.price)}</StyledTableCell>
        </StyledTableRow>
      );
    });
  };

  return (
    <div>
      <div id="tableTitleAlign">
        <h3 id="tableTitle">Sales History</h3>
      </div>
      <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Sales Rep</StyledTableCell>
              <StyledTableCell align="center">Customer Contact</StyledTableCell>
              <StyledTableCell align="center">Vehicle ID No.</StyledTableCell>
              <StyledTableCell align="center">Vehicle</StyledTableCell>
              <StyledTableCell align="center">Sale Price</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>{renderTableRows()}</TableBody>
        </Table>
      </TableContainer>
      <TablePagination
        currentPage={currentPage}
        numPages={Math.ceil(sales.length / itemsPerPage)}
        handlePageChange={handlePageChange}
      />
      <div className="fab-container">
        <Fab color="secondary" aria-label="add" onClick={handleClickOpen}>
          <AddIcon />
        </Fab>
        <Dialog className="glass" open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
          <DialogTitle>Add New Sale</DialogTitle>
          <SalesForm handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
        </Dialog>
      </div>
    </div>
  );
}
