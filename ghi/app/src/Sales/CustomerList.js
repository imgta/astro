import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {useEffect, useState} from 'react';
import { Fab, Button, Dialog, DialogTitle } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import CustomerForm from './CustomerForm';
import '../styles/global.css';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
  '&:hover': {
    backgroundColor: '#16213e',
    boxShadow: '0 2px 5px rgba(0, 0, 0, 0.2)', // Add a box shadow on hover
    cursor: 'pointer',
  },
}));

export default function CustomerList() {
  const [customerData, setCustomerData] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 5;
  const [numPages, setNumPages] = useState(Math.ceil(customerData.length / itemsPerPage));
  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
      setOpen(false);
  };
  const handleFormSubmit = () => {
      loadData();
  };

  async function loadData(){
    const response = await fetch('http://localhost:8090/api/customers');
    if(response.ok){
        const modelData = await response.json();
        setCustomerData(modelData.customers);
        setNumPages(Math.ceil(modelData.customers.length / itemsPerPage));
    } else {
        console.error('Failed to fetch customer data');
    }
  }

  useEffect(()=>{
      loadData();
  }, []);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderTableRows = () => {
    const start = currentPage * itemsPerPage;
    const end = start + itemsPerPage;
    const pageData = customerData.slice(start, end);

    return pageData.map((customer) => (
      <StyledTableRow key={customer.id}>
        <StyledTableCell align="center">{customer.first_name}</StyledTableCell>
        <StyledTableCell align="center">{customer.last_name}</StyledTableCell>
        <StyledTableCell align="center">{customer.phone_number}</StyledTableCell>
        <StyledTableCell align="center">{customer.address}</StyledTableCell>
      </StyledTableRow>
    ));
  };

  return (
    <div>
      <div id="tableTitleAlign">
        <h3 id="tableTitle">Customers</h3>
      </div>
      <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">First Name</StyledTableCell>
              <StyledTableCell align="center">Last Name</StyledTableCell>
              <StyledTableCell align="center">Phone Number</StyledTableCell>
              <StyledTableCell align="center">Address</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderTableRows()}
          </TableBody>
        </Table>
      </TableContainer>

      {numPages > 1 && (
        <div>
          <Button color="secondary" disabled={currentPage === 0} onClick={() => handlePageChange(currentPage - 1)}>
            Previous
          </Button>
          <Button color="secondary" disabled={currentPage === numPages - 1} onClick={() => handlePageChange(currentPage + 1)}>
            Next
          </Button>
        </div>
      )}

      <div className='fab-container'>
        <Fab color="secondary" aria-label="add" onClick={handleClickOpen}>
          <AddIcon />
        </Fab>
        <Dialog open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
          <DialogTitle>Add New Customer</DialogTitle>
          <CustomerForm handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
        </Dialog>
      </div>
    </div>
  );
}
