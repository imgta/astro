import {useState} from 'react';

function RepForm({handleFormSubmit}) {

    const [formData, setData] = useState({
        first_name: "",
        last_name: "",
        employee_id: "",
    });

    const handleChange = (event) => {
        const { name, value } = event.target;

        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));

    }

    // Auto-completes "employee_id" field when first and last name fields are populated, on last name field deselect
    const handleBlur = (event) => {
        const { name, value } = event.target;
        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
        if(formData.first_name && formData.last_name) {
            const lastName = formData.last_name.toLowerCase();
            const firstName = formData.first_name.toLowerCase();
            const employeeId = firstName.charAt(0) + lastName;
            setData(oldData => ({
                ...oldData,
                employee_id: employeeId,
            }));
        };
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};

        try {
            const repUrl = "http://localhost:8090/api/salespeople/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(repUrl, fetchConfig);
            if(!response.ok) {
                throw new Error('Request for sales reps failed.')
            }

            setData({
                first_name: "",
                last_name: "",
                employee_id: "",
            });
            handleFormSubmit();

    } catch (error) {
        console.error(error);
    }
    };


    return (
        <div className="form-container embedform">
            <form onSubmit={handleSubmit} id="create-rep-form">

                <input onChange={handleChange} onBlur={handleBlur} required placeholder="First name" type="text" id="first_name" name="first_name" className="form-control" value={formData.first_name} />

                <input onChange={handleChange} onBlur={handleBlur} required placeholder="Last name" type="text" id="last_name" name="last_name" className="form-control" value={formData.last_name}/>

                <input onChange={handleChange} required placeholder="Employee ID" type="text" id="employee_id" name="employee_id" className="form-control" value={formData.employee_id}/>

                <div className="text-center">
                    <button type="submit" className="btn btn-outline-primary btn-lg btn-block create-button">Create</button>
                </div>
            </form>
        </div>
    );
}

export default RepForm;
