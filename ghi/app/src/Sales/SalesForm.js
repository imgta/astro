import {useEffect, useState} from 'react';
import '../styles/global.css';

function SalesForm({handleFormSubmit}) {

    const [formData, setData] = useState({
        automobiles: [],
        automobile: "",
        reps: [],
        rep: "",
        customers: [],
        customer: "",
        price: "",
    });

    const fetchData = async () => {
        try {
                const autoUrl = "http://localhost:8100/api/automobiles/";
                const repUrl = "http://localhost:8090/api/salespeople/";
                const customerUrl = "http://localhost:8090/api/customers/";

                const autoResponse = await fetch(autoUrl);
                const repResponse = await fetch(repUrl);
                const customerResponse = await fetch(customerUrl);

                if(!autoResponse.ok) {
                    throw new Error('Request for automobile failed.')
                } else if(!repResponse.ok) {
                    throw new Error('Request for sales rep failed.')
                } else if(!customerResponse.ok) {
                    throw new Error('Request for customer failed.')
                }

                const newAuto = await autoResponse.json();
                const newRep = await repResponse.json();
                const newCustomer = await customerResponse.json();

                // Filter and store unsold automobiles in autosInStock
                const autosInStock = newAuto.autos.filter(
                    (auto) => auto.sold === false
                    );

                    setData((prevData) => ({
                        ...prevData,
                        automobiles: autosInStock,
                        reps: newRep.reps,
                        customers: newCustomer.customers,
                        price: "",
                    }));
        } catch (error) {
            console.error(error);
        }}

    useEffect(() => {
        fetchData();
    }, []);

    const handleChange = (event) => {
        const { name, value } = event.target;

        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};
        delete data.automobiles;
        delete data.reps;
        delete data.customers;

        try {
            const salesUrl = "http://localhost:8090/api/sales/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const salesResponse = await fetch(salesUrl, fetchConfig)

            if(!salesResponse.ok) {
                throw new Error('Request for sales record failed.')
            }

            // Update "sold" property of selected automobile to true
            const soldVin = formData.automobile;
            const soldAutoUrl = `http://localhost:8100/api/automobiles/${soldVin}/`;
            const soldAutoConfig = {
                method: "PUT",
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({sold: true}),
            };
            const soldAutoResponse = await fetch(soldAutoUrl, soldAutoConfig);

            if(!soldAutoResponse.ok) {
                throw new Error('Sold status update failed...')
            }
            console.log("Sold status update was successful!", soldAutoResponse);
            window.location.reload();


            const autoSold = formData.automobiles.map(auto => {
                if(auto.vin === formData.automobile) {
                    return { ...auto, sold: true };
                }
                return auto;
            });

            setData((prevData) => ({
                ...prevData,
                automobile: "",
                price: "",
                automobiles: autoSold,
            }));
            handleFormSubmit();

        } catch (error) {
            console.error(error);
        }
    };

    return (
        <div className="form-container embedform">
            <form onSubmit={handleSubmit}>
                <select onChange={handleChange} required placeholder="Automobile" id="automobile" name="automobile" value={formData.automobile} className="form-control mb-3 select-field">
                <option value="">Select automobile</option>
                        {formData.automobiles.map(automobile => {
                        return (
                            <option key={automobile.href} value={automobile.vin}>
                            {automobile.year} {automobile.model.manufacturer.name} {automobile.model.name} ({automobile.color})
                            </option>
                        );
                        })}
                </select>

                <select onChange={handleChange} required placeholder="Sales rep" type="text" id="rep" name="rep" className="form-control mb-3 select-field" value={formData.rep}>
                    <option value="">Select sales rep</option>
                            {formData.reps.map(rep => {
                            return (
                                <option key={rep.id} value={rep.employee_id}>
                                {rep.first_name} {rep.last_name} - {rep.employee_id}
                                </option>
                            );
                            })}
                    </select>
                <select onChange={handleChange} required placeholder="Customer" type="text" id="customer" name="customer" className="form-control mb-3 select-field" value={formData.customer}>
                    <option value="">Select customer</option>
                            {formData.customers.map(customer => {
                            return (
                                <option key={customer.id} value={customer.id}>
                                {customer.first_name} {customer.last_name} - {customer.phone_number}
                                </option>
                            );
                            })}
                </select>
                <input onChange={handleChange} required placeholder="Price" type="text" id="price" name="price" className="form-control" value={formData.price}/>
                <div className="text-center">
                    <button type="submit" className="btn btn-outline-primary btn-lg btn-block create-button">Create</button>
                </div>
            </form>
        </div>
    );
}

export default SalesForm;
