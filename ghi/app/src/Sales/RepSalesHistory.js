import * as React from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import {useEffect, useState} from 'react';


const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
}));

export default function RepSalesHistory() {
    const [sales, setSales] = useState([]);
    const [reps, setReps] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [automobiles, setAutomobiles] = useState([]);
    const [selectRep, setSelectRep] = useState('');
    const [repSales, setRepSales] = useState(0);


    const fetchSalesData = async () => {
        try {
            const salesResponse = await fetch('http://localhost:8090/api/sales/');
            const salesData = await salesResponse.json();
            setSales(salesData.sales);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchRepsData = async () => {
        try {
            const repsResponse = await fetch('http://localhost:8090/api/salespeople/');
            const repsData = await repsResponse.json();
            setReps(repsData.reps);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchCustomers = async () => {
        try {
            const custResponse = await fetch('http://localhost:8090/api/customers/');
            const custData = await custResponse.json();
            setCustomers(custData.customers);
        } catch (error) {
            console.error(error);
        }
    };

    const fetchAutos = async () => {
        try {
            const autosResponse = await fetch('http://localhost:8100/api/automobiles/');
            const autosData = await autosResponse.json();
            setAutomobiles(autosData.automobiles);
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchSalesData();
        fetchRepsData();
        fetchCustomers();
        fetchAutos();
    }, []);

    const handleRepChange = (event) => {
        const value = event.target.value
        setSelectRep(value)
    };

    // Calculate total amount of sales a selected rep has made
    useEffect(() => {
        const totalSales = () => {
            const selectRepSales = sales.filter(sale => sale.rep === selectRep);
            const repTotal = selectRepSales.reduce((total, sale) => total + sale.price, 0);
            setRepSales(repTotal);
        };
        totalSales();
    }, [sales, selectRep]);

    // Delineates large numbers by adding commas
    const numberForm = (val) => {
        return val.toLocaleString();
    };

    return (
    <div>
        <div id="tableTitleAlign">
            <h3 id="tableTitle">Salesperson History</h3>
        </div>
        <div className="mb-3">
            <select onChange={handleRepChange} value={selectRep} required name="rep" id="rep" className="form-select form-select-sm mx-auto">
                <option value="">Choose a sales rep</option>
                {reps.map(rep => (
                <option key={rep.employee_id} value={rep.employee_id}>{rep.employee_id}</option>
                ))}
            </select>
        </div>
        <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
            <TableRow>
                <StyledTableCell align="center">Sales Rep</StyledTableCell>
                <StyledTableCell align="center">Vehicle ID No.</StyledTableCell>
                <StyledTableCell align="center">Customer</StyledTableCell>
                <StyledTableCell align="center">Sale Price</StyledTableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {sales.map(sale => {
                if (sale.rep === selectRep) {

                return (
                <StyledTableRow key={sale.id}>
                <StyledTableCell component="th" scope="row">
                    {sale.id}
                </StyledTableCell>
                <StyledTableCell align="center">{ sale.rep }</StyledTableCell>
                <StyledTableCell align="center">{ sale.automobile }</StyledTableCell>
                <StyledTableCell align="center">{ sale.customer }</StyledTableCell>
                <StyledTableCell align="center">${numberForm(sale.price)}.00</StyledTableCell>
                </StyledTableRow>
                );
            }
            return null;
            })}
            </TableBody>
        </Table>
        </TableContainer>
    </div>
    );
}


// import React, {useEffect, useState} from 'react';

// function RepSalesHistory() {
//     const [sales, setSales] = useState([]);
//     const [reps, setReps] = useState([]);
//     const [customers, setCustomers] = useState([]);
//     const [automobiles, setAutomobiles] = useState([]);
//     const [selectRep, setSelectRep] = useState('');
//     const [repSales, setRepSales] = useState(0);


//     const fetchSalesData = async () => {
//         try {
//             const salesResponse = await fetch('http://localhost:8090/api/sales/');
//             const salesData = await salesResponse.json();
//             setSales(salesData.sales);
//         } catch (error) {
//             console.error(error);
//         }
//     };

//     const fetchRepsData = async () => {
//         try {
//             const repsResponse = await fetch('http://localhost:8090/api/salespeople/');
//             const repsData = await repsResponse.json();
//             setReps(repsData.reps);
//         } catch (error) {
//             console.error(error);
//         }
//     };

//     const fetchCustomers = async () => {
//         try {
//             const custResponse = await fetch('http://localhost:8090/api/customers/');
//             const custData = await custResponse.json();
//             setCustomers(custData.customers);
//         } catch (error) {
//             console.error(error);
//         }
//     };

//     const fetchAutos = async () => {
//         try {
//             const autosResponse = await fetch('http://localhost:8100/api/automobiles/');
//             const autosData = await autosResponse.json();
//             setAutomobiles(autosData.automobiles);
//         } catch (error) {
//             console.error(error);
//         }
//     };

//     useEffect(() => {
//         fetchSalesData();
//         fetchRepsData();
//         fetchCustomers();
//         fetchAutos();
//     }, []);

//     const handleRepChange = (event) => {
//         const value = event.target.value
//         setSelectRep(value)
//     };

//     // Calculate total amount of sales a selected rep has made
//     useEffect(() => {
//         const totalSales = () => {
//             const selectRepSales = sales.filter(sale => sale.rep === selectRep);
//             const repTotal = selectRepSales.reduce((total, sale) => total + sale.price, 0);
//             setRepSales(repTotal);
//         };
//         totalSales();
//     }, [sales, selectRep]);

//     // Delineates large numbers by adding commas
//     const numberForm = (val) => {
//         return val.toLocaleString();
//     };

//     return (
//         <>
//         <br />
//         <h2 className="text-center">Sales Person History</h2>
//         <br />
//         <div className="mb-3">
//             <select onChange={handleRepChange} value={selectRep} required name="rep" id="rep" className="form-select form-select-sm mx-auto">
//                 <option value="">Choose a sales rep</option>
//                 {reps.map(rep => (
//                 <option key={rep.employee_id} value={rep.employee_id}>{rep.employee_id}</option>
//                 ))}
//             </select>
//         </div>
//             <table className="table-fill">

//                 <thead>
//                 <tr>
//                     <th>Sales Rep</th>
//                     <th>Vehicle ID No</th>
//                     <th>Customer</th>
//                     <th>Sale Price</th>
//                 </tr>
//                 </thead>
//                 <tbody className="table-hover">
//                 {sales.map(sale => {
//                     if (sale.rep === selectRep) {
//                     return (
//                     <tr key={sale.id}>
//                         <td>{sale.rep}</td>
//                         <td>{sale.automobile}</td>
//                         <td>{sale.customer}</td>
//                         <td>${numberForm(sale.price)}.00</td>
//                     </tr>
//                     );
//             }
//             return null;
//             })}
//                 </tbody>
//             </table>
//             <br />
//             <div className="text-center"><h5>Total Sales: ${numberForm(repSales)}.00</h5></div>
//         </>
//         );
// }

// export default RepSalesHistory;
