import React, {useState} from 'react';

function CustomerForm({handleFormSubmit}) {
    const [formData, setData] = useState({
        first_name: "",
        last_name: "",
        address: "",
        phone_number: "",
    });

    const handleChange = (event) => {
        const { name, value } = event.target;

        setData(oldData => ({
            ...oldData,
            [name]: value,
        }));
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = { ...formData};

        try {
            const url = "http://localhost:8090/api/customers/";
            const fetchConfig = {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    'Content-Type': 'application/json',
                },
            };

            const response = await fetch(url, fetchConfig);
            if(!response.ok) {
                throw new Error('Request for customers failed.')
            }
            const newCustomer = await response.json();

            setData({
                first_name: "",
                last_name: "",
                address: "",
                phone_number: "",
            });
            handleFormSubmit();

    } catch (error) {
        console.error(error);
    }
    };

    return (
        <div className="form-container embedform">
            <form onSubmit={handleSubmit}>
                <input onChange={handleChange} required placeholder="First name" type="text" id="first_name" name="first_name" className="form-control" value={formData.first_name} />
                <input onChange={handleChange} required placeholder="Last name" type="text" id="last_name" name="last_name" className="form-control" value={formData.last_name}/>
                <input onChange={handleChange} required placeholder="Address" type="text" id="address" name="address" className="form-control" value={formData.address}/>
                <input onChange={handleChange} required placeholder="Phone" type="text" id="phone_number" name="phone_number" className="form-control" value={formData.phone_number}/>
                <div className="text-center">
                    <button type="submit" className="btn btn-outline-primary btn-lg btn-block create-button">Create</button>
                </div>
            </form>
        </div>
    );
}

export default CustomerForm;
