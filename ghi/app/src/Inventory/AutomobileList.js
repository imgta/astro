import { CardContent, Typography, Card, CardHeader } from '@mui/material';
import React, { useState, useEffect } from 'react';
import { Fab } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogTitle from '@mui/material/DialogTitle';
import Box from '@mui/material/Box';
import IconButton from '@mui/material/IconButton';
// import DeleteIcon from '@mui/icons-material/Delete';
import DeleteForeverOutlinedIcon from '@mui/icons-material/DeleteForeverOutlined';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import AutomobileForm from './AutomobileForm';
import '../styles/global.css';

function AutomobileList() {
    const [autoData, setAutoData] = useState([]);
    const [autoColumns, setAutoColumns] = useState([[], [], []]);
    const [open, setOpen] = useState(false);
    const [currentPage, setCurrentPage] = useState(0);
    const itemsPerPage = 6;
    const [numPages, setNumPages] = useState(Math.ceil(autoData.length / itemsPerPage));
    const [deleteDialogOpen, setDeleteDialogOpen] = useState(false);
    const [autoToDelete, setAutoToDelete] = useState(null);

    const handleClickOpen = () => {
        setOpen(true);
    };

    const handleClose = () => {
        setOpen(false);
    };

    const handleFormSubmit = () => {
        loadData();
    };

    const handleDeleteClick = (auto) => {
        setAutoToDelete(auto);
        setDeleteDialogOpen(true);
    };

    const handleConfirmDelete = async () => {
        const response = await fetch(`http://localhost:8100/api/automobiles/${autoToDelete.vin}`, {
        method: 'DELETE',
        });
        if (response.ok) {
            loadData();
        } else {
            console.error('Failed to delete automobile');
        }
        setDeleteDialogOpen(false);
        setAutoToDelete(null);
    };

    const handleCancelDelete = () => {
        setDeleteDialogOpen(false);
        setAutoToDelete(null);
    };

    async function loadData() {
        const response = await fetch('http://localhost:8100/api/automobiles/');
        if (response.ok) {
            const modelData = await response.json();
            setAutoData(modelData.autos);
            setNumPages(Math.ceil(modelData.autos.length / itemsPerPage));
        } else {
            console.error('Failed to fetch automobile data');
        }
    }

    useEffect(() => {
        loadData();
    }, []);

    useEffect(() => {
        const start = currentPage * itemsPerPage;
        const end = start + itemsPerPage;
        const pageData = autoData.slice(start, end);
        const columns = [[], [], []];
        pageData.forEach((auto, index) => {
            columns[index % 3].push(auto);
        });
        setAutoColumns(columns);
    }, [autoData, currentPage]);


    return (
        <>
        <div className="text-center">
            <h2>Vehicles</h2>

        <Box component="span">
            {autoColumns.some((column) => column.length > 0) ? (
            <div className="container">
                {autoColumns.map((column, index) => (
                <div key={index} className="col">
                    {column.map((auto) => {
                        var currencyFormat;
                        if(auto.price > 999999.99){
                            currencyFormat = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                notation: 'compact',
                                compactDisplay: 'short',
                                minimumFractionDigits: 1,
                                maximumFractionDigits: 2,
                            }).format(auto.price)
                            console.log(typeof(currencyFormat))
                        }else{
                            currencyFormat = new Intl.NumberFormat('en-US', {
                                style: 'currency',
                                currency: 'USD',
                                maximumFractionDigits: 0,
                        }).format(auto.price.toFixed(0))
                        }
                        return(
                        <div className="card-container" style={{ marginTop: '25px', marginLeft: '0px', position: 'relative', width: '209px', height: '262px'}}>
                            <svg class="rectangle-3" width="209" height="262" viewBox="0 0 209 262" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M208 261H1V131V65.5V33.097L14.4596 16.0356L26.6028 1H52.25H104.5H208V261Z" fill="url(#paint0_linear_1_125)" stroke="url(#paint1_linear_1_125)" stroke-width="2"></path>
                                <defs>
                                <linearGradient id="paint0_linear_1_125" x1="104.5" y1="0" x2="104.5" y2="262" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="#707070"></stop>
                                    <stop offset="1" stop-color="#707070" stop-opacity="0"></stop>
                                </linearGradient>
                                <linearGradient id="paint1_linear_1_125" x1="104.5" y1="0" x2="105" y2="162" gradientUnits="userSpaceOnUse">
                                    <stop stop-color="white"></stop>
                                    <stop offset="1" stop-color="white" stop-opacity="0"></stop>
                                </linearGradient>
                                </defs>
                            </svg>
                            <div className="ship-price" style={{position: 'absolute', top: '17px', right: '36px'}}>
                                Price: {currencyFormat}
                            </div>
                            <div className="picture-box" style={{position: 'absolute', bottom: '50px', left: '50%', transform: 'translate(-50%, -50%)'}}>
                                <img src={auto.model.picture_url} style={{objectFit: 'cover', width: '100%', height: '100%'}} loading="lazy" alt="" />
                            </div>
                            <div className="ship-name" style={{position: 'absolute', bottom: '75px', left: '35px'}}>
                                {auto.model.name}
                            </div>
                            <div className="build" style={{position: 'absolute', bottom: '57px', left: '35px'}}>
                            by {auto.model.manufacturer.name}
                            </div>
                            <div className="year" style={{position: 'absolute', bottom: '43px', left: '35px'}}>
                            sol: {auto.year}
                            </div>
                            <div style={{position: 'absolute', bottom: '10px', right: '10px'}} className="delete-icon">
                                <IconButton
                                    className="delete-icon"
                                    aria-label="delete"
                                    onClick={() => handleDeleteClick(auto)}
                                >
                                <DeleteForeverOutlinedIcon style={{color: "#b9b9b9e3"}} className="delete-icon"/>
                                </IconButton>
                            </div>
                        </div>
                    );})}
                </div>
                ))}
            </div>
            ) : (
            <Typography variant="body1" id="missing-ships">There are no ships available right now.</Typography>
            )}

            {autoColumns.some((column) => column.length > 0) && (
            <div>
                <Button color="secondary" disabled={currentPage === 0} onClick={() => setCurrentPage(currentPage - 1)}>
                Previous
                </Button>
                <Button color="secondary" disabled={currentPage === numPages - 1} onClick={() => setCurrentPage(currentPage + 1)}>
                Next
                </Button>
            </div>
            )}

            <div className="fab-container">
            <Fab color="secondary" aria-label="add" className="new-button" onClick={handleClickOpen}>
                <AddIcon />
            </Fab>
            <Dialog className="glass" open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
                <DialogTitle>Add New Ship</DialogTitle>
                <AutomobileForm handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
            </Dialog>
            <Dialog open={deleteDialogOpen} onClose={handleCancelDelete} aria-labelledby="alert-dialog-title" aria-describedby="alert-dialog-description">
                <DialogTitle id="alert-dialog-title">{"Confirm Delete"}</DialogTitle>
                <DialogContent>
                <DialogContentText id="alert-dialog-description">Are you sure you want to delete this spaceship?</DialogContentText>
                </DialogContent>
                <DialogActions>
                <Button onClick={handleCancelDelete} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleConfirmDelete} color="primary" autoFocus>
                    Delete
                </Button>
                </DialogActions>
            </Dialog>
            </div>
        </Box>
        </div>
        </>
    );
}

export default AutomobileList;
