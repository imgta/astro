import React, {useState, useEffect} from 'react'
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
    [`&.${tableCellClasses.head}`]: {
      backgroundColor: theme.palette.common.black,
      color: theme.palette.common.white,
    },
    [`&.${tableCellClasses.body}`]: {
      fontSize: 14,
    },
  }));

  const StyledTableRow = styled(TableRow)(({ theme }) => ({
    '&:nth-of-type(odd)': {
      backgroundColor: theme.palette.action.hover,
    },
    // hide last border
    '&:last-child td, &:last-child th': {
      border: 0,
    },
  }));


async function loadData() {
        const response = await fetch('http://localhost:8100/api/manufacturers/');
        if (response.ok) {
            const data = await response.json();
            return data.manufacturers;
        } else {
            console.error('Failed to fetch manufacturer data');
            return [];
        }
}

    function ManufacturerList() {
        const [manufacturerData, setManufacturerData] = useState([]);

        useEffect(() => {
        loadData().then(data => {
            setManufacturerData(data);
        });
        }, []);

        return (
            <div>
        <div id="tableTitleAlign">
            <h3 id="tableTitle">Manufacturers</h3>
        </div>
        <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
            <TableHead>
            <TableRow>
                <StyledTableCell align="center">Name</StyledTableCell>
            </TableRow>
            </TableHead>
            <TableBody>
            {manufacturerData.map((manufacturer) => (
                <StyledTableRow key={manufacturer.id}>
                <StyledTableCell component="th" scope="row" align="center">
                    {manufacturer.name}
                </StyledTableCell>
                </StyledTableRow>
            ))}
            </TableBody>
        </Table>
        </TableContainer>
    </div>
  );

}
    export default ManufacturerList;
