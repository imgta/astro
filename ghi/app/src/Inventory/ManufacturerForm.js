import React, {useEffect,useState} from 'react'

function ManufacturerForm(){
    const[manufacturer, setManufacturer] = useState('');

    const handleManufacturerChange = (event) => {
        const value = event.target.value;
        setManufacturer(value);
    }

    const fetchManuData = async() => {
        const url = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(url);

        if (response.ok){
            const manuData = await response.json()  ;
            setManufacturer(manuData.manufacturer);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            name: manufacturer,
        };
        const manufacturerUrl = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(manufacturerUrl, fetchConfig);
        if (response.ok) {
            setManufacturer('');
        }
    };

    useEffect(() =>{
        fetchManuData();
    }, []);

    return (
        <div className="form-container embedform">
    <form onSubmit={handleSubmit} id="create-manufacturer-form">
    <div className="row mb-3">
    <div className="col">
        <div className="form-floating mb-3">
        <input
            onChange={handleManufacturerChange}
            required
            placeholder="manufacturer"
            type="text"
            id="manufacturer"
            name="manufacturer"
            className="form-control"
            value={manufacturer}
        />
        <label htmlFor="manufacturer">Manufacturer Name</label>
        </div>
    </div>
    </div>
    <div className="text-center">
    <button type="submit" className="btn btn-outline-primary btn-lg btn-block create-button mb-10">
        Create
    </button>
    </div>
</form>
</div>
    );
}

export default ManufacturerForm
