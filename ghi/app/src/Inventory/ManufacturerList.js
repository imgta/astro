import React, { useEffect, useState } from 'react';
import CardScript from './util/card';
import './css/cards.css';
import { svgLogos } from './imgs/svgExport';
import { Fab, Button, Dialog, DialogTitle } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import ManufacturerForm from './ManufacturerForm';


function ManufacturersList() {
    const [manufacturers, setManufacturers] = useState([]);
    const [manufactModels, setManufactModels] = useState({});
    const [models, setModels] = useState([]);
    const [open, setOpen] = useState(false);

    const handleClickOpen = () => {
        setOpen(true);
    };
    const handleClose = () => {
        setOpen(false);
    };
    const handleFormSubmit = () => {
        fetchManufacturers();
    };

    const fetchManufacturers = async () => {
        try {
            const url = 'http://localhost:8100/api/manufacturers/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setManufacturers(data.manufacturers);
            }
        } catch (error) {
            console.error(error);
        }
    };

    const fetchModels = async () => {
        try {
            const url = 'http://localhost:8100/api/models/';
            const response = await fetch(url);

            if (response.ok) {
                const data = await response.json();
                setModels(data.models);

                // Group car models by their manufacturer
                const modelManufact = {};
                data.models.forEach(model => {
                    const manufactName = model.manufacturer.name;
                    if(!modelManufact[manufactName]) {
                        modelManufact[manufactName] = [];
                    }
                    modelManufact[manufactName].push(model.name);
                });
                setManufactModels(modelManufact);
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchModels();
        fetchManufacturers();
    }, []);


    return (
<>
        <div className="text-center">
            <h2>Manufacturers</h2>

        <div className="cards">
        {manufacturers.map(manufacturer => {
            const logoName = manufacturer.name.replace(/\s+/g, '');
            const LoadLogo = svgLogos[logoName];
    return (
    <div className="card" key={manufacturer.id}>

        <div className="card__image-holder">
            <LoadLogo />
        </div>


        <div className="card-title" style={{color: "black"}}>{manufacturer.name}</div>

        {/* <div className="card-flap flap1">
            <div className="card-description">
            </div>
            <div className="text-primary fst-italic lh-1 fw-medium">
            <br />
            </div>
            <div className="card-flap flap2"></div>
        </div> */}
    </div>
        );
        })}
        <CardScript />
        </div>
        <div className="fab-container">
        <Fab color="secondary" aria-label="add" onClick={handleClickOpen}>
        <AddIcon />
        </Fab>
        <Dialog className="glass" open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
        <DialogTitle>Add Manufacturer</DialogTitle>
        <ManufacturerForm align="center" handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
        </Dialog>
    </div>
        </div>
</>
    );
}


export default ManufacturersList;
