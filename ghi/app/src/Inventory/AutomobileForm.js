import React, {useState, useEffect} from 'react'


function AutomobileForm({ handleClose, handleFormSubmit }){
    const[color, setColor] = useState('');
    const[year, setYear] = useState('');
    const[vin, setVin] = useState('');
    const[price, setPrice] = useState('');
    const[model, setModel] = useState('');
    const[models, setModels] = useState([]);

    const handleColorChange = (event) => {
        const value = event.target.value;
        setColor(value);
    }

    const handleYearChange = (event) => {
        const value = event.target.value;
        setYear(value);
    }

    const handleVinChange = (event) => {
        const value = event.target.value;
        setVin(value);
    }

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleModelChange = (event) => {
        const value = event.target.value;
        setModel(value);
    }

    const fetchData = async() => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);

        if (response.ok){
            const data = await response.json()  ;
            setModels(data.models);
        }
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.color = color;
        data.year = year;
        data.vin = vin;
        data.model_id = model;
        data.price = price
        const autoUrl = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            }
        }
        const response = await fetch(autoUrl, fetchConfig);
        if (response.ok){
            handleFormSubmit();  // add this line
            setColor('');
            setYear('');
            setVin('');
            setModel('');
            setPrice('');
        }
    }

    useEffect(() =>{
        fetchData();
    }, []);

    return (
        <div className="form-container embedform">
            <form onSubmit={handleSubmit}>
                <input onChange={handleColorChange} required placeholder="Color" type="text" id="color" name="color" className="form-control" value={color} />
                <input onChange={handleYearChange} required placeholder="Year" type="text" id="year" name="year" minLength='4' maxLength="4" className="form-control" value={year}
                onKeyDown={ (event) => {
                    if (!/[0-9]/.test(event.key) && event.key !== 'Backspace' && event.key !== 'Delete') {
                        event.preventDefault();
                    }
                }}/>
                <input onChange={handleVinChange} required placeholder="VIN" type="text" id="vin" name="vin" minLength='17' maxLength="17" className="form-control" value={vin}/>
                <input onChange={handlePriceChange} required placeholder="Price" type="text" id="price" name="price" className="form-control" value={price} />
                <select onChange={handleModelChange} name="model" id="model" required value={model} className="form-select mb-3 select-field">
                    <option value="">Choose a Model</option>
                    {models.map((model) =>(
                        <option key={model.id} value={model.id}> {model.name} </option>
                    ))}
                </select>

                <div className="text-center">
                    <button type="submit" className="btn btn-outline-primary btn-lg btn-block create-button">Create</button>
                </div>
            </form>
        </div>
    );
}

export default AutomobileForm;
