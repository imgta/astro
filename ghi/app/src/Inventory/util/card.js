import { useEffect } from 'react';

function CardScript() {
useEffect(() => {
const handleClick = (e) => {
    const card = e.target.closest('.card');
    if (!card) return;

    e.preventDefault();
    const isShowing = card.classList.contains('show');
    const cardsContainer = document.querySelector('.cards');

    if (cardsContainer.classList.contains('showing')) {
    // A card is already in view
    const showingCard = cardsContainer.querySelector('.card.show');
    showingCard?.classList.remove('show');

    if (isShowing) {
        // This card was showing - reset the grid
        cardsContainer.classList.remove('showing');
    } else {
        // This card isn't showing - show it
        card.style.zIndex = 20;
        card.classList.add('show');
    }
    } else {
    // No cards in view
    cardsContainer.classList.add('showing');
    card.style.zIndex = 20;
    card.classList.add('show');
    }
};

document.addEventListener('click', handleClick);

return () => {
    document.removeEventListener('click', handleClick);
};
}, []);

return null;
}

export default CardScript;
