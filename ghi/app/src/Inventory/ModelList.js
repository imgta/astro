import React, { useState, useEffect } from 'react';
import { styled } from '@mui/material/styles';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell, { tableCellClasses } from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Fab, Button, Dialog, DialogTitle } from '@mui/material';
import AddIcon from '@mui/icons-material/Add';
import ModelForm from './ModelForms';
import '../styles/global.css';

const StyledTableCell = styled(TableCell)(({ theme }) => ({
  [`&.${tableCellClasses.head}`]: {
    backgroundColor: theme.palette.common.black,
    color: theme.palette.common.white,
  },
  [`&.${tableCellClasses.body}`]: {
    fontSize: 14,
  },
}));

const StyledTableRow = styled(TableRow)(({ theme }) => ({
  '&:nth-of-type(odd)': {
    backgroundColor: theme.palette.action.hover,
  },
  // hide last border
  '&:last-child td, &:last-child th': {
    border: 0,
  },
  '&:hover': {
    // boxShadow: "inset 0px 0px 0px 2px rgba(255,255,255,1)"
    backgroundColor: "rgba(93, 63, 211, 0.25)"
  },
}));

export default function ModelList() {
  const [modelData, setModelData] = useState([]);
  const [open, setOpen] = useState(false);
  const [currentPage, setCurrentPage] = useState(0);
  const itemsPerPage = 5;
  const [numPages, setNumPages] = useState(Math.ceil(modelData.length / itemsPerPage));

  const handleClickOpen = () => {
    setOpen(true);
  };

  const handleClose = () => {
    setOpen(false);
  };

  const handleFormSubmit = () => {
    loadData();
  };

  async function loadData() {
    const response = await fetch('http://localhost:8100/api/models');
    if (response.ok) {
      const modelData = await response.json();
      setModelData(modelData.models);
      setNumPages(Math.ceil(modelData.models.length / itemsPerPage));
    } else {
      console.error('Failed to fetch model data');
    }
  }

  useEffect(() => {
    loadData();
  }, []);

  const handlePageChange = (page) => {
    setCurrentPage(page);
  };

  const renderTableRows = () => {
    const start = currentPage * itemsPerPage;
    const end = start + itemsPerPage;
    const pageData = modelData.slice(start, end);

    return pageData.map((model) => (
      <StyledTableRow key={model.id}>
        <StyledTableCell align="center">{model.name}</StyledTableCell>
        <StyledTableCell align="center">{model.manufacturer.name}</StyledTableCell>
        <StyledTableCell align="center">
          <img
            src={model.picture_url}
            alt={model.name}
            style={{ width: '150px', height: '100px', objectFit: 'cover', border: '1px solid #fff' }}
          />
        </StyledTableCell>
      </StyledTableRow>
    ));
  };

  return (
    <div>
      <div id="tableTitleAlign">
        <h3 id="tableTitle">Models</h3>
      </div>
      <TableContainer component={Paper} id="table">
        <Table sx={{ minWidth: 700 }} aria-label="customized table">
          <TableHead>
            <TableRow>
              <StyledTableCell align="center">Name</StyledTableCell>
              <StyledTableCell align="center">Manufacturer</StyledTableCell>
              <StyledTableCell align="center">Picture</StyledTableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {renderTableRows()}
          </TableBody>
        </Table>
      </TableContainer>

      {numPages > 1 && (
        <div>
          <Button color="secondary" disabled={currentPage === 0} onClick={() => handlePageChange(currentPage - 1)}>
            Previous
          </Button>
          <Button color="secondary" disabled={currentPage === numPages - 1} onClick={() => handlePageChange(currentPage + 1)}>
            Next
          </Button>
        </div>
      )}

      <div className="fab-container">
        <Fab color="secondary" aria-label="add" onClick={handleClickOpen}>
          <AddIcon />
        </Fab>
        <Dialog className="glass" open={open} onClose={handleClose} PaperProps={{ className: 'glass-dialog' }}>
          <DialogTitle>Add New Model</DialogTitle>
          <ModelForm align="center" handleClose={handleClose} handleFormSubmit={handleFormSubmit} />
        </Dialog>
      </div>
    </div>
  );
}
