// import { Typography } from '@mui/material';
// import purple from '@mui/material/colors/purple';
import carverseImg from './images/astro.png';


function MainPage() {
  return (

    <div className="px-4 py-5 my-5 text-center" style={{margin:"0 auto"}}>
      <img src={carverseImg} alt="Carverse" className="pb-4" />
      {/* <h1 className="display-5 fw-bold">Carverse</h1> */}
      <br/>
        <p className="lead mb-4 pt-4">
          Infinite Choices, Stellar Service!
        </p>
    </div>
  );
}

export default MainPage;
// sx={{ color: purple[500] }}
